
<aside class="left-sidebar">
  <!-- Sidebar scroll-->
  <div class="scroll-sidebar">
    <nav class="sidebar-nav">
      <ul id="sidebarnav">
        <!-- User Profile-->
        <li>
          <!-- User Profile-->
          <div class="user-profile dropdown m-t-20">
            <div class="user-pic">
              <img src="{{url('dashboard_assets/default.png')}}" alt="users" class="rounded-circle img-fluid" />
            </div>
            <div class="user-content hide-menu m-t-10">
              <h5 class="m-b-10 user-name font-medium">{{$user->name}}</h5>
              <a href="javascript:void(0)" class="btn btn-circle btn-sm m-r-5" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false">
              <i class="ti-settings"></i>
            </a>
            <a title="Logout" class="btn btn-circle btn-sm" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="fa fa-power-off"></i>
            </a>
            <div class="dropdown-menu animated flipInY" aria-labelledby="Userdd">
              <a class="dropdown-item" href="javascript:void(0)">
                <i class="ti-user m-r-5 m-l-5"></i> My Profile
              </a>
              <a class="dropdown-item" href="javascript:void(0)">
                <i class="ti-wallet m-r-5 m-l-5"></i> My Balance
              </a>
              <a class="dropdown-item" href="javascript:void(0)">
                <i class="ti-email m-r-5 m-l-5"></i> Inbox
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="javascript:void(0)">
                <i class="ti-settings m-r-5 m-l-5"></i> Account Setting
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off m-r-5 m-l-5"></i> Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>

            </div>
          </div>
        </div>
        <!-- End User Profile-->
      </li>
      <!-- User Profile-->
      <li class="nav-small-cap">
        <i class="mdi mdi-dots-horizontal"></i>
        <span class="hide-menu">CRM Management</span>
      </li>
      <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
          <i class="icon-Car-Wheel"></i>
          <span class="hide-menu">Companies </span>
        </a>
        <ul aria-expanded="false" class="collapse  first-level">
          <li class="sidebar-item">
            <a href="{{url('dashboard/company')}}" class="sidebar-link">
              <i class="icon-Record"></i>
              <span class="hide-menu"> List All Companies </span>
            </a>
          </li>
          <li class="sidebar-item">
            <a href="{{url('dashboard/company/create')}}" class="sidebar-link">
              <i class="icon-Record"></i>
              <span class="hide-menu"> Add New Company</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="sidebar-item">
        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
          <i class="icon-Car-Wheel"></i>
          <span class="hide-menu">Employees </span>
        </a>
        <ul aria-expanded="false" class="collapse  first-level">
          <li class="sidebar-item">
            <a href="{{url('dashboard/employee')}}" class="sidebar-link">
              <i class="icon-Record"></i>
              <span class="hide-menu"> List All Employees </span>
            </a>
          </li>
          <li class="sidebar-item">
            <a href="{{url('dashboard/employee/create')}}" class="sidebar-link">
              <i class="icon-Record"></i>
              <span class="hide-menu"> Add New Employee</span>
            </a>
          </li>
        </ul>
      </li>

    </ul>
  </nav>
  <!-- End Sidebar navigation -->
</div>
<!-- End Sidebar scroll-->
</aside>
<!-- Sidebar navigation-->
