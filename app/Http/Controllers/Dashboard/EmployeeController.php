<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Employee;
use App\Company;

class EmployeeController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $data['employees'] = Employee::paginate(10);
    return view('dashboard.employee.index', $data);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $data['companies'] = Company::all();
    return view('dashboard.employee.create', $data);
  }

  /**
  * filter a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(EmployeeRequest $request)
  {
    Employee::insertRow($request);
    return redirect('dashboard/employee')->with('msg', 'Employee added successfully.');
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $data['employee'] = Employee::findOrFail($id);
    $data['companies'] = Company::all();
    return view('dashboard.employee.edit', $data);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(EmployeeRequest $request, $id)
  {
    Employee::updateRow($request, $id);
    return redirect()->back()->with('msg', 'Employee updated successfully.');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    Employee::findOrFail($id)->delete();
    return ['status'=>1, 'msg'=>'Employee deleted successfully.'];

  }
}
