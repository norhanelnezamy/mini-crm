<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Company;
use App\Employee;

class CompanyController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $data['companies'] = Company::paginate(10);
    return view('dashboard.company.index', $data);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('dashboard.company.create');
  }

  /**
  * filter a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $data['employees'] = Employee::where('company_id', $id)->paginate(10);
    return view('dashboard.employee.index', $data);
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(CompanyRequest $request)
  {
    Company::insertRow($request);
    return redirect('dashboard/company')->with('msg', 'Company added successfully.');
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $company = Company::findOrFail($id);
    return view('dashboard.company.edit', compact('company', 'roles'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(CompanyRequest $request, $id)
  {
    Company::updateRow($request, $id);
    return redirect()->back()->with('msg', 'Company updated successfully.');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    Company::findOrFail($id)->delete();
    return ['status'=>1, 'msg'=>'Company deleted successfully.'];

  }
}
