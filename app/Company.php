<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Company extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = "companies";

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'name', 'website', 'email', 'logo'
  ];

  /**
  * insert new company row.
  */
  public static function insertRow($request)
  {
    $company = new Company();
    $company->email = $request->email;
    $company->name = $request->name;
    $company->website = $request->website;
    if ($request->has('logo')) {
      $company->logo = $request->file('logo')->store('public/logos');
    }
    $company->save();
  }

  /**
  * update company row.
  */
  public static function updateRow($request, $id)
  {
    $company = Company::findOrFail($id);
    $company->email = $request->email;
    $company->name = $request->name;
    $company->website = $request->website;
    if ($request->has('logo')) {
      if (Storage::exists($company->logo)) {
        Storage::delete($company->logo);
      }
      $company->logo = $request->file('logo')->store('public/logos');
    }
    $company->save();
  }

  /**
  * Get the companys of company.
 */
 public function company()
 {
   return $this->hasMany('App\Employee');
 }

}
