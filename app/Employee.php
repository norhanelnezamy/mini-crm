<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = "employees";

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'first_name', 'last_name', 'email', 'company_id', 'phone'
  ];

  /**
  * insert new employee row.
  */
  public static function insertRow($request)
  {
    $employee = new Employee();
    $employee->email = $request->email;
    $employee->first_name = $request->first_name;
    $employee->last_name = $request->last_name;
    $employee->phone = $request->phone;
    $employee->company_id = $request->company_id;
    $employee->save();
  }

  /**
  * update employee row.
  */
  public static function updateRow($request, $id)
  {
    $employee = Employee::findOrFail($id);
    $employee->email = $request->email;
    $employee->first_name = $request->first_name;
    $employee->last_name = $request->last_name;
    $employee->phone = $request->phone;
    $employee->company_id = $request->company_id;
    $employee->save();
  }

  /**
  * Get the company that owns the employee.
 */
 public function company()
 {
   return $this->belongsTo('App\Company');
 }

}
